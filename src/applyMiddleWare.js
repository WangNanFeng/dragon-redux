// redux中的 applyMiddleWare
// 只是用来加工dispatch的工厂,而加工什么样的dispatch出来,则需要我们传入对应的中间件函数

// 中间件 - Redux把这个封装的入口写成了一个函数，就叫applyMiddleware。
// 由此我们明白了applyMiddleware的功能：改造dispatch函数，产生真假dispatch，
// 而中间件就是运行在假真之间的代码

// 精简版applyMiddleWare

const applyMiddleWare = function(middleWare) {
  let next = store.dispatch
  store.dispacth = middleWare(store)(next) // 这里传入store，是因为中间件中有可能会用到getState获取数据，比如打印当前用户等需求
}

// 中间件的串联融合

const logger = store => next => action => {
  console.log('dispatching1: ', action)
  return next(action)
}

// action => {
//   console.log('dispatching1: ', action)
//   return store.dispacth(action)
// }

//  action => {
//   try {
//     console.log('dispatching2: ', action)
//     console.log('dispatching1: ', action)
//     return store.dispacth(action)
//   } catch (err) {
//     console.error('Error!', err)
//   }
// }


const collectError = store => next => action => {
  try {
    console.log('dispatching2: ', action)
    return next(action)
  } catch (err) {
    console.error('Error!', err)
  }
}

function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg
  }

  if (funcs.length === 1) {
    return funcs[0]
  }

  const last = funcs[funcs.length - 1]
  const rest = funcs.slice(0, -1)
  return (...args) => rest.reduceRight((composed, f) => f(composed), last(...args))
}

console.log(
  compose(
    logger,
    collectError
  )
)
function applyMiddleware(...middlewares) {
  return createStore => (reducer, preloadedState, enhancer) => {
    var store = createStore(reducer, preloadedState, enhancer)
    var dispatch = store.dispatch
    var chain = []

    var middlewareAPI = {
      getState: store.getState,
      dispatch: action => dispatch(action)
    }
    chain = middlewares.map(middleware => middleware(middlewareAPI))
    dispatch = compose(...chain)(store.dispatch)

    return {
      ...store,
      dispatch
    }
  }
}
// 改造applyMiddleWare

// function applyMiddleWare(middleWares) {
//   middleWares = middleWares.slice()
//   middleWares.reverse()

//   let dispatch = store.dispatch
//   middleWares.forEach(middleWare => {
//     dispatch = middleWare(store)(dispatch)
//   })

//   return Object.assign({}, store, { dispacth })
// }

// 上面的middleware(store)(dispatch) 就相当于是 const logger = store => next => {}，
// 这就是构造后的dispatch，继续向下传递。
// 这里middlewares.reverse()，进行数组反转的原因，是最后构造的dispatch，实际上是最先执行的。
// 因为在applyMiddleware串联的时候，每个中间件只是返回一个新的dispatch函数给下一个中间件，实际上这个dispatch并不会执行。
// 只有当我们在程序中通过store.dispatch(action)，真正派发的时候，才会执行。
// 而此时的dispatch是最后一个中间件返回的包装函数。然后依次向前递推执行

function create(reducer, enhancer) {
  if (enhancer) {
    return enhancer
  }
  console.log(reducer(1))
}

create(function() {
  console.log('reducer')
})



