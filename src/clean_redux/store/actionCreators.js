import * as types from './actionTypes'

export const add = () => ({ type: types.ADD, count: 1 })

export const remove = () => ({ type: types.REMOVE, count: 1 })

export const async = () => dispatch => {
  setTimeout(() => {
    dispatch(add())
  }, 2000)
}
export const addTwo = () => [add(), async()]