import React, { Component } from 'react'
import propTypes from 'prop-types'
import { bindActionCreators } from '../dragon_redux'

export default (mapStateToProps, mapDispatchToProps) => WrapComponent => {
  return class extends Component {
    static contextTypes = {
      store: propTypes.object
    }

    constructor(props, context) {
      super(props, context)
      this.state = {
        props: {}
      }
    }

    update() {
      const { store } = this.context

      // 合并数据
      const stateProps = mapStateToProps(store.getState())

      // 合并方法
      let mapDispatch = mapDispatchToProps ? mapDispatchToProps : {}
      let dispatchProps
      if (typeof mapDispatch !== 'object' && typeof mapDispatch !== 'function') {
        throw new Error('Expected the listener to be a function or object')
      } else if (typeof mapDispatch === 'object') {
        dispatchProps = bindActionCreators(mapDispatch, store.dispatch)
      } else if (typeof mapDispatch === 'function') {
        dispatchProps = mapDispatch(store.dispatch)
      }

      this.setState({
        props: {
          ...this.state.props,
          ...stateProps,
          ...dispatchProps
        }
      })
    }

    componentDidMount() {
      const { store } = this.context
      store.subscribe(() => this.update())
      this.update()
    }

    render() {
      return <WrapComponent {...this.state.props} />
    }
  }
}
