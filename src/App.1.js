import React, { Component } from 'react'
import { connect } from './dragon-react-redux'
import { Button } from 'antd'
import * as actionCreators from './store/actionCratros'
// import { add, remove } from './store/actionCratros'

class App extends Component {
  render() {
    const { count, add, remove, async, addTwo } = this.props
    return (
      <div style={{ margin: '100px 100px' }}>
        count: {count}
        <br />
        <Button type="primary" onClick={add}>
          ADD
        </Button>
        <Button type="primary" onClick={remove}>
          REMOVE
        </Button>
        <Button type="primary" onClick={async}>
          ASYNC
        </Button>
        <Button type="primary" onClick={addTwo}>
          ADDTWO
        </Button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  count: state
})

const mapDispatchToProps = dispatch => ({
  add() {
    dispatch(actionCreators.add())
  },
  remove() {
    dispatch(actionCreators.remove())
  },
  async() {
    dispatch(actionCreators.async())
  },
  addTwo() {
    dispatch(actionCreators.addTwo())
  }
})

// const mapDispatchToProps = { add, remove }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
