import { createStore, applyMiddleware } from '../dragon-redux'
// import * as actionCreators from './actionCratros'

import reducer from './reducer'
import { thunk, thunkArray } from '../thunk'

const store = createStore(reducer, applyMiddleware(thunk, thunkArray))

function listenr() {
  console.log('TCL: listenr -> store.getState ', store.getState())
}

store.subscribe(listenr)

export default store
